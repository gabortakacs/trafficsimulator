﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AICar : MonoBehaviour 
{

	[SerializeField] private Car _car;
	[SerializeField] private Transform _roadHelper;
	[SerializeField] private Transform _rotationHelper;

	public RoadmapRoad _goalRoad;
	public RoadmapWay _currentWay;

	private Stack<RoadmapWay> _path;
	private Stack<Transform> _insidePointStack;
	private bool _found = false;

	public RoadmapWay CurrentWay
	{
		set
		{
			_currentWay = value;
			if (_currentWay.InsidePoints.Length == 0)
				_insidePointStack = new Stack<Transform>();
			else
				_insidePointStack = new Stack<Transform>(_currentWay.InsidePoints);
		}
	}

	private void Update()
	{
		if(Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.Q))
		{
			CalculatePath();
		}

		if (!_found)
			return;

		Vector3 roadHelperPos = _roadHelper.transform.position;
		roadHelperPos.y = _car.transform.position.y;
		_roadHelper.transform.position = roadHelperPos;
		_rotationHelper.LookAt(_roadHelper);
	}

	private void FixedUpdate()
	{
		if (!_found)
			return;
		Quaternion newRotation = Quaternion.RotateTowards(_car.Rigidbody.rotation, _rotationHelper.rotation, _car.RotationSpeed * Time.fixedDeltaTime);
		_car.Rigidbody.MoveRotation(newRotation);
		_car.Rigidbody.velocity = _car.transform.forward * _car.MaxSpeed;
	}

	public void CalculatePath()
	{
		_found = RoadmapManager.Instance.GetRoadPath(_currentWay, _goalRoad, out _path);
		if (!_found)
			return;

		CurrentWay = _path.Pop();
		SetRoadHelperToNextPos();
	}

	public void WaypointReached()
	{
		if (Vector3.Distance(_currentWay.Exit.transform.position,_roadHelper.transform.position) < 0.1f	)
		{
			if (_path.Count > 0)
				CurrentWay = _path.Pop();
			else
			{
				_found = false;
				_car.Rigidbody.velocity = Vector3.zero;
			}
		}
		SetRoadHelperToNextPos();
	}

	private void SetRoadHelperToNextPos()
	{
		if (_insidePointStack.Count == 0)
			_roadHelper.transform.position = _currentWay.Exit.transform.position;
		else
			_roadHelper.transform.position = _insidePointStack.Pop().position;
	}
}

