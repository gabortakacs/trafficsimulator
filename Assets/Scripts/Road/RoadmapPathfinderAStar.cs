﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RoadmapPathfinderAStar : RoadmapPathfinder
{
	public bool GetRoadPath(RoadmapRoad start, RoadmapRoad end, out Stack<RoadmapWay> path)
	{
		var noteMapHelper = new Dictionary<RoadmapWay, Note>();
		var openList = CreateOpenList(start, noteMapHelper);
		Note endNote;
		bool found = PathfinderAStart(openList, noteMapHelper, end, out endNote);
		if (found)
		{
			DecryptionPath(endNote, noteMapHelper, start, out path);
			return true;
		}
		else
		{
			path = new Stack<RoadmapWay>();
			return false;
		}
	}

	public bool GetRoadPath(RoadmapWay startWay, RoadmapRoad end, out Stack<RoadmapWay> path)
	{
		var noteMapHelper = new Dictionary<RoadmapWay, Note>();
		var openList = new List<Note>();
		Note note = new Note(startWay);
		note.DistanceFromStart = startWay.Length;
		noteMapHelper[note.Way] = note;
		openList.Add(note);
		Note endNote;
		bool found = PathfinderAStart(openList, noteMapHelper, end, out endNote);
		if (found)
		{
			DecryptionPath(endNote, noteMapHelper, startWay, out path);
			return true;
		}
		else
		{
			path = new Stack<RoadmapWay>();
			return false;
		}
	}
	private bool PathfinderAStart(List<Note> openList, Dictionary<RoadmapWay, Note> noteMapHelper, RoadmapRoad end, out Note endNote)
	{
	//	throw new NotImplementedException();
	//}

	//private bool PathfinderAStart(RoadmapRoad start, RoadmapRoad end, out LinkedList<RoadmapWay> path)
	//{
	//	var noteMapHelper = new Dictionary<RoadmapWay, Note>();
	//	var openList = CreateOpenList(start, noteMapHelper);
		var closedSet = new HashSet<RoadmapWay>();
		bool found = false;
		Note currentNote = new Note();

		int counter = 0;
		while (openList.Count > 0)
		{
			if(counter++ > 500)
			{
				Debug.Log("<color=blue>counter is more than 500</color>");
				break;
			}
			SortList(openList, end);
			currentNote = PollFirstElemetFromList(openList);
			if(currentNote.Way.OwnerRoad == end)
			{
				found = true;
				break;
			}
			closedSet.Add(currentNote.Way);
			var connectedLink = currentNote.Way.Exit.ConnectedLink;
			if (connectedLink == null)
				continue;
			var neighbors = connectedLink.Road.GetRoadmapWaysByRoadmapRoadLink(currentNote.Way.Exit.ConnectedLink);
			foreach(RoadmapWay neighbor in neighbors)
			{
				if (closedSet.Contains(neighbor))
					continue;
				Note neighborNote = GetAndSetNeighborNote(neighbor, currentNote, noteMapHelper);
				openList.Add(neighborNote);
			}
		}
		endNote = currentNote;
		return found;
	}

	private bool DecryptionPath(Note currentNote, Dictionary<RoadmapWay, Note> noteMapHelper, RoadmapRoad start, out Stack<RoadmapWay> path)
	{
		path = new Stack<RoadmapWay>();
		while (currentNote.Way.OwnerRoad != start)
		{
			path.Push(currentNote.Way);
			if (!noteMapHelper.TryGetValue(currentNote.Parent, out currentNote))
			{
				Debug.Log("<color=red>ERROR! Parent not found</color>");
				return false;
			}
		}
		path.Push(currentNote.Way);
		return true;
	}

	private bool DecryptionPath(Note currentNote, Dictionary<RoadmapWay, Note> noteMapHelper, RoadmapWay startWay, out Stack<RoadmapWay> path)
	{
		path = new Stack<RoadmapWay>();
		while (currentNote.Way != startWay)
		{
			path.Push(currentNote.Way);
			if (!noteMapHelper.TryGetValue(currentNote.Parent, out currentNote))
			{
				Debug.Log("<color=red>ERROR! Parent not found</color>");
				return false;
			}
		}
		path.Push(currentNote.Way);
		return true;
	}

	private Note GetAndSetNeighborNote(RoadmapWay neighbor, Note currentNote, Dictionary<RoadmapWay, Note> noteMapHelper)
	{
		Note neighborNote;
		if (noteMapHelper.TryGetValue(neighbor, out neighborNote))
		{
			if (neighborNote.DistanceFromStart > currentNote.DistanceFromStart + neighbor.Length)
				neighborNote.DistanceFromStart = currentNote.DistanceFromStart + neighbor.Length;
		}
		else
		{
			neighborNote = new Note(neighbor, currentNote.Way);
			neighborNote.DistanceFromStart = currentNote.DistanceFromStart + neighbor.Length;
			noteMapHelper[neighbor] = neighborNote;
		}
		return neighborNote;
	}

	private Note PollFirstElemetFromList(List<Note> openList)
	{
		var note = openList[0];
		openList.RemoveAt(0);
		return note;
	}

	private List<Note> CreateOpenList(RoadmapRoad start, Dictionary<RoadmapWay, Note> noteMapHelper)
	{
		var openList = new List<Note>();
		foreach (RoadmapWay way in start.Ways)
		{
			Note note = new Note(way);
			note.DistanceFromStart = way.Length;
			noteMapHelper[note.Way] = note;
			openList.Add(note);
		}
		return openList;
	}

	private float CalcHeuristics(RoadmapWay way, RoadmapRoad end)
	{
		return Vector3.Distance(way.Exit.transform.position, end.transform.position);
	}

	private void SortList(List<Note> list, RoadmapRoad end)
	{
		list.Sort(
			delegate (Note r1, Note r2)
			{
				float r1Dist = r1.DistanceFromStart + CalcHeuristics(r1.Way, end);
				float r2Dist = r2.DistanceFromStart + CalcHeuristics(r2.Way, end);
				return r1Dist.CompareTo(r2Dist);
			}
		);
	}

	private struct Note
	{
		public RoadmapWay Way;
		public RoadmapWay Parent;
		public float DistanceFromStart;

		public Note(RoadmapWay way, RoadmapWay parent = null)
		{
			Way = way;
			Parent = parent;
			DistanceFromStart = float.PositiveInfinity;
		}
	}

}

