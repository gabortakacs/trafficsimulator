﻿using System.Collections.Generic;
using UnityEngine;
//using Random = UnityEngine.Random;

public class RoadmapManager : Singleton<RoadmapManager> 
{
	[SerializeField] private RoadmapPathfinderType _pathfinderType;
	private RoadmapPathfinder _pathfinder;

	private HashSet<RoadmapRoad> _roads = new HashSet<RoadmapRoad>();
#if UNITY_EDITOR
	public bool DrawWayLines = true;
#endif

	private void Awake()
	{
		_pathfinder = new RoadmapPathfinderAStar();
	}

	public void AddRoad(RoadmapRoad road)
	{
		_roads.Add(road);
	}

	public void RemoveRoad(RoadmapRoad road)
	{
		_roads.Remove(road);
	}

	public bool GetRoadPath(RoadmapRoad start, RoadmapRoad end, out Stack<RoadmapWay> path)
	{
		return _pathfinder.GetRoadPath(start, end, out path);
	}

	public bool GetRoadPath(RoadmapWay startWay, RoadmapRoad end, out Stack<RoadmapWay> path)
	{
		return _pathfinder.GetRoadPath(startWay, end, out path);
	}
}
