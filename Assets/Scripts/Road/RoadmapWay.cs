﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class RoadmapWay
{
	[SerializeField] private RoadmapRoadLink _entrance;
	[SerializeField] private RoadmapRoadLink _exit;
	[SerializeField] private Transform[] _insidePoints;
	[SerializeField] private RoadmapRoad _owner;

	public RoadmapRoadLink Entrance { get { return _entrance; } }
	public RoadmapRoadLink Exit { get { return _exit; } }
	public Transform[] InsidePoints { get { return _insidePoints; } }
	public RoadmapRoad OwnerRoad { get { return _owner; } }
	public float Length { get; private set; }

	public RoadmapWay(RoadmapRoadLink entrace, RoadmapRoadLink exit, RoadmapRoad owner)
	{
		_entrance = entrace;
		_exit = exit;
		_owner = owner;
		Length = Vector3.Distance(entrace.transform.position, exit.transform.position);
	}

#if UNITY_EDITOR
	static Dictionary<RoadmapRoadLink, Color> colors = new Dictionary<RoadmapRoadLink, Color>();
	public void DebugDraw()
	{
		if(Entrance == null || Exit == null)
			return;
		Color color;
		if (!colors.TryGetValue(Entrance, out color))
		{
			color = Random.ColorHSV();
			colors[Entrance] = color;
		}

		Debug.DrawLine(Entrance.transform.position, Exit.transform.position, color);
	}
#endif
}