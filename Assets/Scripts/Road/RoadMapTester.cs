﻿using UnityEngine;
using System.Collections.Generic;

public class RoadmapTester : MonoBehaviour
{
	public RoadmapRoad START;
	public RoadmapWay STARTWAY;
	public RoadmapRoad END;
	public Color[] colors;

	Stack<RoadmapWay> list = null;
	private void Update()
	{
		//if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.X))
		//{
		//	if (RoadmapManager.Instance.GetRoadPath(START, END, out list))
		//	{
		//		int counter = 0;
		//		var currentNode = list.First;
		//		while (currentNode != null)
		//		{
		//			Debug.Log(++counter + "/" + list.Count + ": " + currentNode.Value.Entrance.Road.transform.parent.name + "-" +currentNode.Value.Entrance.Road.name);
		//			currentNode = currentNode.Next;
		//		}
		//	}
		//}
		if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Y))
		{
			RoadmapManager.Instance.GetRoadPath(STARTWAY, END, out list);
		}
		DrawPath(list);
	}

	public void DrawPath(Stack<RoadmapWay> path)
	{
		if (path == null)
			return;
		int colorCounter = 0;
		while (path.Count > 0)
		{
			var currentNode = path.Pop();
			if (currentNode == null)
				break;

			Debug.DrawLine(currentNode.Entrance.transform.position, currentNode.Exit.transform.position, colors[colorCounter]);
			colorCounter = colorCounter == colors.Length - 1 ? 0 : ++colorCounter;
		}
	}
}

