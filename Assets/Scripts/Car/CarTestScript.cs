﻿using System.Collections;
using UnityEngine;

public class CarTestScript : MonoBehaviour 
{
	private CarController _car;
	private bool rotate = false;
	private void Awake()
	{
		_car = GetComponent<CarController>();
	}

	private void Update()
	{
		if (Input.GetKeyDown("[7]"))
		{
			_car.PreDirection = CarDirection.Left;
		}
		else if (Input.GetKeyDown("[9]"))
		{
			_car.PreDirection = CarDirection.Right;
		}
		else if (Input.GetKeyDown("[2]"))
		{
			_car.PreDirection = CarDirection.None;
		}



		if (Input.GetKeyDown("[8]"))
		{
			_car.Direction = CarDirection.Forward;
		}
		else if (Input.GetKeyDown("[4]"))
		{
			_car.Direction = CarDirection.Left;
		}
		else if (Input.GetKeyDown("[5]"))
		{
			_car.Direction = CarDirection.Backwards;
		}
		else if (Input.GetKeyDown("[6]"))
		{
			_car.Direction = CarDirection.Right;
		}
		else if (Input.GetKeyDown("[0]"))
		{
			_car.Direction = CarDirection.None;
		}
		if (Input.GetKeyDown("[1]"))
		{
			rotate = !rotate;
			Debug.Log(rotate);
		}
		if(rotate)
			_car.Direction = CarDirection.Right;
	}
}

