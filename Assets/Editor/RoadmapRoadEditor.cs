﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(RoadmapRoad))]
public class RoadmapRoadEditor : Editor 
{

	RoadmapRoad road;
	bool delete = false;
	bool[] toggles;
	private void OnEnable()
	{
		road = (target as RoadmapRoad);
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (road == null || road.Ways == null)
			return;
		if (GUILayout.Button("Calculate all waypoint"))
		{
			road.CalculateAllPossibleWays();
		}
		if(GUILayout.Button("Delete ways"))
		{
			delete = !delete;
		}

		if (delete)
		{
			if(toggles == null || toggles.Length != road.Ways.Length)
				toggles = new bool[road.Ways.Length];
			for(int i = 0; i < road.Ways.Length; ++i)
			{
				toggles[i] = EditorGUILayout.Toggle(new GUIContent("Ways " + i, road.Ways[i].Entrance.name + "->" + road.Ways[i].Exit.name), toggles[i]);
			}
			var deleteIndexes = new HashSet<int>();
			for(int i = 0; i < toggles.Length; ++i)
			{
				if (toggles[i])
					deleteIndexes.Add(i);
			}
			if (GUILayout.Button("Delete"))
			{
				road.DeleteWays(deleteIndexes);
			}
		}
	}



}

