﻿using System;
using System.Collections;
using UnityEngine;

public class CarController : MonoBehaviour
{
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private TurnSign _leftTurnSign;
	[SerializeField] private TurnSign _rightTurnSign;
	[SerializeField] private WheelController _wheelController;
	[SerializeField] private float _maxSpeed;
	[SerializeField] private float _rotationDuration;

	private CarDirection _lastDirection = CarDirection.None;
	private CarDirection _direction = CarDirection.None;
	private CarDirection _preDirection = CarDirection.None;

	private bool _isTurning = false;

	private float _speed = 0;
	public float _rotationAmount = 90f;

	public float RotationAmount { set { _rotationAmount = value; } }
	public float Speed
	{
		set
		{
			_speed = value;
			ChangeWheelsSpeed(_speed);
		}
	}

	public CarDirection PreDirection
	{
		get
		{
			return _preDirection;
		}
		set
		{
			_preDirection = value;
			switch (_preDirection)
			{
				case CarDirection.Left:
					_leftTurnSign.IsOn = true;
					_rightTurnSign.IsOn = false;
					break;
				case CarDirection.Right:
					_leftTurnSign.IsOn = false;
					_rightTurnSign.IsOn = true;
					break;
				default:
					_leftTurnSign.IsOn = false;
					_rightTurnSign.IsOn = false;
					break;
			}
		}
	}

	public CarDirection Direction
	{
		set
		{
			_direction = value;
		}
	}

	private void Update()
	{
		float velocityY = _rigidbody.velocity.y;
		Vector3 newVelocity = transform.forward * _speed;
		newVelocity.y += velocityY;
		_rigidbody.velocity = newVelocity;
		if (_direction != _lastDirection)
			ChangeDirection();
		_lastDirection = _direction;
		
	}

	private void ChangeDirection()
	{
		if (_isTurning)
			return;
		switch (_direction)
		{
			case CarDirection.None:
				Speed = 0;
				break;
			case CarDirection.Forward:
				Speed = _maxSpeed;
				break;
			case CarDirection.Backwards:
				Speed = -_maxSpeed;
				break;
			case CarDirection.Left:
				Speed = _maxSpeed;
				RotateCar(_lastDirection);
				break;
			case CarDirection.Right:
				Speed = _maxSpeed;
				RotateCar(_lastDirection);
				break;
		}
	}

	private void RotateCar(CarDirection lastDirection)
	{
		if (_isTurning)
			return;

		StartCoroutine(RotateCarRoutine(lastDirection));
	}

	private IEnumerator RotateCarRoutine(CarDirection lastDirection)
	{
		_isTurning = true;
		float dir = (_direction == CarDirection.Right ? 1 : -1);
		float timer = 0;
		float rotationAmountPerSec = _rotationAmount / _rotationDuration;
		float finalRotationAngleY = transform.rotation.eulerAngles.y + _rotationAmount * dir;
		while(timer < _rotationDuration)
		{
			float step = (timer + Time.deltaTime) > _rotationDuration ? _rotationDuration - timer : Time.deltaTime;
			Vector3 rotation = Quaternion.AngleAxis(step * rotationAmountPerSec, Vector3.up).eulerAngles * dir;
			transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + rotation);
			timer += Time.deltaTime;
			yield return null;
		}
		Vector3 finalRotation = transform.rotation.eulerAngles;
		finalRotation.y = finalRotationAngleY;
		transform.rotation = Quaternion.Euler(finalRotation);
		_isTurning = false;
		_direction = lastDirection;
	}

	private void ChangeWheelsSpeed(float newSpeed)
	{
		_wheelController.RotationSpeed = newSpeed;
	}
}

