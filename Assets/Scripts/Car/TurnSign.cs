﻿using UnityEngine;

public class TurnSign : MonoBehaviour
{
	[SerializeField] private Renderer _renderer;
	[SerializeField] private Material _material;
	[SerializeField] private string _onShaderString = "Boolean_64AAEA5E";
	[SerializeField] private string _delayShaderString = "Vector1_3A87BAA7";

	private bool _isOn = false;

	public bool IsOn {
		get { return _isOn; }
		set
		{
			_isOn = value;
			_renderer.material.SetInt(_onShaderString, _isOn ? 1:0);
			_renderer.material.SetFloat(_delayShaderString, Time.time);
		}
	}

	private void Awake()
	{
		_renderer.material = Instantiate(_material);
	}
}

