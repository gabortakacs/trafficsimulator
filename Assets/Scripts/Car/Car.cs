﻿using UnityEngine;

public enum CarDirection
{
	None = 0,
	Forward = 1,
	Backwards = 2,
	Left = 3,
	Right = 4
}

public class Car : MonoBehaviour
{
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private TurnSign _leftTurnSign;
	[SerializeField] private TurnSign _rightTurnSign;
	[SerializeField] private float _maxSpeed = 5;
	[SerializeField] private float _rotationSpeed = 30;

	public Rigidbody Rigidbody { get { return _rigidbody; } }
	public float MaxSpeed { get { return _maxSpeed; } }
	public float RotationSpeed { get { return _rotationSpeed; } }


	private CarDirection _preDirection = CarDirection.None;

	public CarDirection PreDirection
	{
		get
		{
			return _preDirection;
		}
		set
		{
			_preDirection = value;
			switch (_preDirection)
			{
				case CarDirection.Left:
					_leftTurnSign.IsOn = true;
					_rightTurnSign.IsOn = false;
					break;
				case CarDirection.Right:
					_leftTurnSign.IsOn = false;
					_rightTurnSign.IsOn = true;
					break;
				default:
					_leftTurnSign.IsOn = false;
					_rightTurnSign.IsOn = false;
					break;
			}
		}
	}
}
