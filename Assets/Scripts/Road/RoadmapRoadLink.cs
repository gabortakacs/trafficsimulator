﻿using UnityEngine;

public enum RoadmapRoadLinkType
{
	In = 1,
	Out = 2
}

public class RoadmapRoadLink : MonoBehaviour
{
	[SerializeField] private RoadmapRoad _road;
	[SerializeField] private RoadmapRoadLinkType _type;

	public RoadmapRoadLink _connectedLink;

	public RoadmapRoadLinkType Type { get { return _type; } }
	public RoadmapRoad Road { get { return _road; } }
	public RoadmapRoadLink ConnectedLink { get { return _connectedLink; } }

	/*
		OutLink fuck the InLink and the InLink is engaged herself by Outlink 
	*/
	private void OnTriggerEnter(Collider other)
	{
		RoadmapRoadLink otherRoadInLink = other.GetComponent<RoadmapRoadLink>();
		if (otherRoadInLink == null || _type == otherRoadInLink.Type)
			return;
		_connectedLink = otherRoadInLink;
	}

	private void OnTriggerExit(Collider other)
	{
		RoadmapRoadLink otherRoadInLink = other.GetComponent<RoadmapRoadLink>();
		if (otherRoadInLink == null || otherRoadInLink != _connectedLink)
			return;
		_connectedLink = null;
	}
}

