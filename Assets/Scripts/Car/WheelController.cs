﻿using UnityEngine;

public class WheelController : MonoBehaviour 
{
	[SerializeField] private Transform[] _wheels;
	[SerializeField] private float _rotationSpeedMultiplier = 0.1f;
	private float _rotationSpeed = 0;

	public float RotationSpeed { set { _rotationSpeed = value; } }

	private void Update()
	{
		if (_rotationSpeed != 0)
		{
			Vector3 rotationAngle = Quaternion.AngleAxis(_rotationSpeed * _rotationSpeedMultiplier * 360 * Time.deltaTime, Vector3.back).eulerAngles;
			for(int i = 0; i < _wheels.Length; ++i)
			{
				_wheels[i].rotation = Quaternion.Euler(_wheels[i].rotation.eulerAngles + rotationAngle);
			}
		}
	}



}

