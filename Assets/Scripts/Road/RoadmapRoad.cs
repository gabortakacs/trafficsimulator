﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class RoadmapRoad : MonoBehaviour 
{
	[SerializeField] private RoadmapRoadLink[] _roadmapLinkIn;
	[SerializeField] private RoadmapRoadLink[] _roadmapLinkOut;
	[SerializeField] private RoadmapWay[] _ways;


	private Dictionary<RoadmapRoadLink, List<RoadmapWay>> _wayMap = new Dictionary<RoadmapRoadLink, List<RoadmapWay>>();

	public RoadmapWay[] Ways { get { return _ways; } }

	private void OnEnable()
	{
		RoadmapManager.Instance.AddRoad(this);
	}

	private void Awake()
	{
		CreateWayMap();
	}

#if UNITY_EDITOR
	private void Update()
	{
		DrawRoadmapWays();
	}
#endif

	public List<RoadmapWay> GetRoadmapWaysByRoadmapRoadLink(RoadmapRoadLink link)
	{
		List<RoadmapWay> list;
		if (_wayMap.TryGetValue(link, out list))
			return list;
		return null;
	}

	private void OnDisable()
	{
		if (!RoadmapManager.IsNull)
			RoadmapManager.Instance.RemoveRoad(this);
	}

	private void CreateWayMap()
	{
		for (int i = 0; i < _ways.Length; ++i)
		{
			List<RoadmapWay> currentList;
			if (!_wayMap.TryGetValue(_ways[i].Entrance, out currentList))
				currentList = new List<RoadmapWay>();

			currentList.Add(_ways[i]);
			_wayMap[_ways[i].Entrance] = currentList;
		}
	}

#if UNITY_EDITOR
	private void DrawRoadmapWays()
	{
		if(!RoadmapManager.Instance.DrawWayLines)
			return;
		foreach (RoadmapWay way in _ways)
		{
			way.DebugDraw();
		}
	}

	public void CalculateAllPossibleWays()
	{
		_ways = new RoadmapWay[_roadmapLinkIn.Length * _roadmapLinkOut.Length];
		for (int i = 0; i < _roadmapLinkIn.Length; ++i)
		{
			for (int j = 0; j < _roadmapLinkOut.Length; ++j)
			{
				_ways[i * _roadmapLinkOut.Length + j] = new RoadmapWay(_roadmapLinkIn[i], _roadmapLinkOut[j], this);
			}
		}
	}

	public void DeleteWays(HashSet<int> indexes)
	{
		RoadmapWay[] _newWays = new RoadmapWay[_ways.Length - indexes.Count];
		int j = 0;
		for (int i = 0; i < _ways.Length; ++i)
		{
			if (!indexes.Contains(i))
			{
				_newWays[j] = _ways[i];
				j++;
			}
		}
		_ways = _newWays;
	}
#endif
}
