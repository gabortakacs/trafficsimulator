﻿using UnityEngine;

public abstract class ObjectPoolEntity : MonoBehaviour 
{

	protected ObjectPool _owner;

	public ObjectPool Owner { get { return _owner; } set { _owner = value; } }

	public abstract void Reset();
}

