﻿using UnityEngine;
using System.Collections;

public class CarWaypointHelper : MonoBehaviour
{

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer != LayerMask.NameToLayer("Car"))
			return;

		AICar car = other.GetComponent<AICar>();
		if(car == null)
		{
			Debug.Log("<color=red>ERROR! AICar is not found!");
			return;
		}
		car.WaypointReached();
	}
}
