﻿using UnityEngine;
using System.Collections.Generic;

public enum RoadmapPathfinderType
{
	AStar
}

public interface RoadmapPathfinder
{
	bool GetRoadPath(RoadmapRoad start, RoadmapRoad end, out Stack<RoadmapWay> path);
	bool GetRoadPath(RoadmapWay startWay, RoadmapRoad end, out Stack<RoadmapWay> path);
}